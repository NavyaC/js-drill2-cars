
const problem1Fn = require('../problem1.cjs');

const data = require('../inventory.cjs');

test("testing problem1", () => {
    expect(problem1Fn(data,33)).toStrictEqual({ "car_make": "Jeep", "car_model": "Wrangler", "car_year": 2011, "id": 33})
});
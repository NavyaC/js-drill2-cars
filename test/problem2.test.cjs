const problem2Fn = require('../problem2.cjs');

const data = require('../inventory.cjs');

test("testing problem2", () => {
    expect(problem2Fn(data)).toEqual({"car_make": "Lincoln", "car_model": "Town Car", "car_year": 1999, "id": 50});
});

const problem5Fn = require('../problem5.cjs');

const data = require('../inventory.cjs');

test("testing problem5", () => {
    expect(problem5Fn(data)).toEqual(25);
});
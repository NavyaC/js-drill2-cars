const inventory = require('./inventory.cjs');

function problem3(inventory) {
    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }
    let arrCars = inventory.map((val) => val.car_model);    
    return arrCars.sort();
};

module.exports = problem3;

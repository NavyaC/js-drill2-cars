const inventory = require('./inventory.cjs');

function problem6(inventory) {
    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }
    let arrList = inventory.filter((ele) => ele.car_model === 'BWM' || ele.car_model === 'Audi');
    return JSON.stringify(arrList);
};

module.exports = problem6;

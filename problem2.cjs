const inventory = require('./inventory.cjs');

const lastId = inventory[inventory.length - 1].id;

function problem2(inventory) {
    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }
    return inventory.filter((val) => {
        return val.id === lastId;
    })[0];
};


module.exports = problem2;

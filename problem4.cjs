const inventory = require('./inventory.cjs');

function problem4(inventory) {
    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }
    let arrYears = inventory.map((val) => val.car_year);
    return arrYears;
};


module.exports =problem4;

const inventory = require('./inventory.cjs');

function problem5(inventory) {
    if (arguments < 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }
    let arrCars = inventory.filter((val) => val.car_year < 2000);
    return(arrCars.length);
}

module.exports = problem5;
